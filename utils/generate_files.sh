#!/bin/bash

# Generate a few txt files
# Generate .o and .pdf files
mkdir -p images/cats/kittens
for folder in images images/cats images/cats/kittens
do
    for _ in {1..5}
    do
        touch $folder/$RANDOM.pdf
    done
done
