#!/bin/bash

REMOTE=remote
make_fake_remote_repo() {
    # Make sure there are no fake remote yet
    rm -rf $REMOTE

    # Initialize empty remote
    git init --bare $REMOTE
}

change_origin() {
    # Remove GitLab remote and replace it with a fake one
    git remote remove origin
    git remote add origin $REMOTE
    git push origin main
}
