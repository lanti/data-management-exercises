# Exercises for the Git introduction 

To prepare your environment for the exercises, proceed as follows:

1. Clone the repository:
   ```
   git clone https://gitlab.epfl.ch/lanti/data-management-exercises.git
   ```
2. Go into the exercise folder and initialize it:
   ```
   cd data-management-exercises
   ./setup.sh
   ```
